defmodule SleepwebWeb.PageController do
  use SleepwebWeb, :controller

  require Logger

  alias Sleepweb.SMS


  def index(conn, _params) do
  # def index(conn, %{"id" => id}) do

    cur_time = NaiveDateTime.utc_now

    data_bpm = SMS.get_bpm_data("sensor02", NaiveDateTime.add(cur_time, -60*60, :second), cur_time)
    data_lin = SMS.get_lin_data("sensor02", NaiveDateTime.add(cur_time, -60*60, :second), cur_time)
    data_ang = SMS.get_ang_data("sensor02", NaiveDateTime.add(cur_time, -60*60, :second), cur_time)


    data_bpm = Enum.reverse(data_bpm)
    data_lin = Enum.reverse(data_lin)
    data_ang = Enum.reverse(data_ang)
    # data_bpm = SMS.latest_bpm_data()
    # data_lin = SMS.latest_lin_data()
    # data_ang = SMS.latest_ang_data()




    render(conn, "index.html", data_bpm: data_bpm, data_lin: data_lin, data_ang: data_ang)





  end

end
