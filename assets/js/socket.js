// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket} from "phoenix"


import moment from 'moment'
import Chart from 'chart.js'

let socket = new Socket("/socket", {params: {token: window.userToken}})

// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/2" function
// in "lib/web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, pass the token on connect as below. Or remove it
// from connect if you don't care about authentication.

socket.connect()

// Now that you are connected, you can join channels with a topic:

let channel           = socket.channel("room:lobby", {})
//let chatInput         = document.querySelector("#chat-input")
//let messagesContainer = document.querySelector("#messages")

// chatInput.addEventListener("keypress", event => {
//   if(event.keyCode === 13){
//     channel.push("new_msg", {body: chatInput.value})
//     chatInput.value = ""
//   }
// })


var timerange_select = document.getElementById('timerange_select');

timerange_select.onchange = function() {
  console.log("Changed timerange.")
  var selection_time = document.getElementById("timerange_select").value;
  var selection_sensor = document.getElementById("sensor_select").value;
  channel.push("requests", {sensor: selection_sensor, timerange: selection_time})
 }


 var sensor_select = document.getElementById('sensor_select');

 sensor_select.onchange = function() {
   console.log("Changed sensor.")
    var selection_time = document.getElementById("timerange_select").value;
    var selection_sensor = document.getElementById("sensor_select").value;
    channel.push("requests", {sensor: selection_sensor, timerange: selection_time})
  }


channel.on("requests", payload => {
  if (payload != undefined)
  {
    payload = payload.data

    if (payload[0] == undefined){
      document.getElementById('status').innerHTML = 'no data available for that timeslot and sensor. \n charts were not updated.';
    }

    if (payload[0].bpm != undefined){
      //document.getElementById('latest_bpm').innerHTML = payload[0].bpm;
      document.getElementById('status').innerHTML = '';

      document.getElementById('latest_bpm').innerHTML = payload[0].bpm;

      BPMChart.data.labels = payload.map(x => x.time)
      BPMChart.data.datasets[0].data = payload.map(x => x.bpm)
      BPMChart.update()
    }

    if (payload[0].lin_acc != undefined){
      document.getElementById('status').innerHTML = '';

      AccelChart.data.labels = payload.map(x => x.time)
      AccelChart.data.datasets[0].data = payload.map(x => x.lin_acc)
      AccelChart.update()
    }

    if (payload[0].angular_change != undefined){
      document.getElementById('status').innerHTML = '';

      AngularChart.data.labels = payload.map(x => x.time)
      AngularChart.data.datasets[0].data = payload.map(x => x.angular_change)
      AngularChart.update()
    }
  }
}
)



channel.on("new_msg", payload => {


  // chatbox
  // let messageItem = document.createElement("li")
  // var now = new Date()
  // messageItem.innerText = `${now.getUTCHours()+':'+ now.getUTCMinutes()+':'+now.getUTCSeconds()} ${JSON.stringify(payload)}`
  // messagesContainer.appendChild(messageItem)

  // only publish the new data if it is from the selected sensor
  console.log(payload)
  if(payload.sensor == document.getElementById("sensor_select").value)
  {
    // set latest BPM at the top

    // only when BPM exists, otherwise no update
    if (payload.bpm != undefined) {
      document.getElementById('latest_bpm').innerHTML = payload.bpm;
      payload.bpm && addData(BPMChart, "BPM", payload.time, payload.bpm);
    }


    payload.lin_acc && addData(AccelChart, "Linear Acceleration", payload.time, payload.lin_acc);

    payload.angular_change && addData(AngularChart, "Angular Change", payload.time, payload.angular_change);

  }
})

channel.join()
  .receive("ok", resp => { console.log("Joined successfully.", resp) })
  .receive("error", resp => { console.log("Unable to join.", resp) })


function addData(chart, dataset_label, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
      if (dataset.label == dataset_label) {
        dataset.data.push(data);
      }
    });
    chart.update();
}

function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

export default socket
