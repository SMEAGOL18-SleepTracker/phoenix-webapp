defmodule SleepwebWeb.RoomChannel do
  use Phoenix.Channel

  require Logger

  alias Sleepweb.SMS

  def join("room:lobby", _message, socket) do
    {:ok, socket}
  end


  def join("room:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end


  def handle_in("new_msg", %{"body" => body}, socket) do
    broadcast! socket, "new_msg", %{body: body}
    {:noreply, socket}
  end

  def handle_in("requests", %{"timerange" => timerange, "sensor" => sensor}, socket) do

    cur_time = NaiveDateTime.utc_now

    {timerange, _} = Integer.parse(timerange)

    Logger.info "Requesting Data for Sensor: #{sensor} in a Timerange of: #{timerange} seconds"


    data_bpm = SMS.get_bpm_data(sensor, NaiveDateTime.add(cur_time, -timerange, :second), cur_time)
    data_lin = SMS.get_lin_data(sensor, NaiveDateTime.add(cur_time, -timerange, :second), cur_time)
    data_ang = SMS.get_ang_data(sensor, NaiveDateTime.add(cur_time, -timerange, :second), cur_time)

    data_bpm = Enum.reverse(data_bpm)
    data_lin = Enum.reverse(data_lin)
    data_ang = Enum.reverse(data_ang)

    push(socket, "requests", %{data: data_bpm})
    push(socket, "requests", %{data: data_lin})
    push(socket, "requests", %{data: data_ang})

    {:noreply, socket}
  end


  def handle_out(event, payload, socket) do
    push socket, event, payload
    {:noreply, socket}
  end

end
