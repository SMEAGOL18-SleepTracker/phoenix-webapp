# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :sleepweb,
  ecto_repos: [Sleepweb.Repo]

# Configures the endpoint
config :sleepweb, SleepwebWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XT7b0s+ezX5xlEahh7AaiHUt/npEdtDe4XwXZsL0ga9GzzZvSqbH7Uw/KNvNqUI2",
  render_errors: [view: SleepwebWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Sleepweb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
