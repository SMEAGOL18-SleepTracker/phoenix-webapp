defmodule Sleepweb.ReleaseTasks do
  def migrate do
    {:ok, _} = Application.ensure_all_started(:ecto)
    {:ok, _} = Application.ensure_all_started(:postgrex)
    Application.load(:sleepweb)
    Sleepweb.Repo.start_link()

    path = Application.app_dir(:sleepweb, "priv/repo/migrations")

    Ecto.Migrator.run(Sleepweb.Repo, path, :up, all: true)

    IO.puts "Migration completed!"

    :init.stop()
  end
end
