defmodule Sleepweb.Mqtt do
  use GenMQTT
  require Logger
  alias Sleepweb.SMS

  def start_link do
    Logger.info "MQTT: Trying to connect..."
    GenMQTT.start_link(__MODULE__, nil, [host: 'sleep.heim.xyz', port: 1883, username: 'sensor01', password: 'heartratesensor'])
  end

  def on_connect(state) do
    Logger.info "MQTT: Connected!"
    :ok = GenMQTT.subscribe(self(), "room/+/temp", 0)
    :ok = GenMQTT.subscribe(self(), "#", 0)
    {:ok, state}
  end

  def on_connect_error(reason, state) do
    Logger.error "MQTT: 'on_connect_error' was triggered. Reason: #{reason}."
    {:ok, state}
  end

  def on_disconnect(state) do
    Logger.info "MQTT: Disconnected! State: #{state}"
    {:ok, state}
  end

  def on_subscribe([{topic, qos}], state) do
    Logger.info "MQTT: Just subscribed to #{topic} with a QoS of #{qos}."
    {:ok, state}
  end


  def on_publish(["room", location, "temp"], message, state) do
    Logger.info "MQTT: It is #{message} degrees in #{location}."
    {:ok, state}
  end


  def on_publish(topic, message, state) do
    Logger.info "MQTT: Topic: #{inspect(topic)} and Message: #{message}."

    # sensor_data = Poison.decode!(message)
    #
    # Logger.info "JSON Decode: #{inspect(sensor_data)}"
    #
    # SleepwebWeb.Endpoint.broadcast! "room:lobby", "new_msg", sensor_data
    # SleepWeb.Endpoint.broadcast! "room:lobby", "new_data", sensor_data

    # Sleepweb.SMS.create_data(sensor_data)
    Logger.info "MQTT: Forwading data to SMS for parsing ..."


    {ret_val, state} = Sleepweb.SMS.parse_data(topic, message)

    # forward the data to SMS which processes, decodes and publishes (websocket and db) the data

    Logger.info "MQTT: SMS.parse_data returned: Return Value: #{inspect(ret_val)} with State: #{inspect(state)}"

    {:ok, state}
  end


end
