#! /usr/local/bin/python3

import json
import time
from random import randint

seed_data = []

for i in range(0, 500):
    dataset = {}
    dataset['time'] = int(time.time()) + i*10
    dataset['bpm'] = randint(40,80)
    dataset['lin_acc'] = randint(0,50)
    dataset['angular_change'] = randint(0,360)
    seed_data.append(dataset)





print(json.dumps(seed_data))
