defmodule Sleepweb.SMS.Tracker do
  use Ecto.Schema
  import Ecto.Changeset


  schema "trackers" do
    field :misc, :map
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(tracker, attrs) do
    tracker
    |> cast(attrs, [:name, :misc])
    |> validate_required([:name])
  end
end
