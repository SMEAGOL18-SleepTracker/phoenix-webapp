defmodule Sleepweb.SMSTest do
  use Sleepweb.DataCase

  alias Sleepweb.SMS

  describe "trackers" do
    alias Sleepweb.SMS.Tracker

    @valid_attrs %{misc: %{}, name: "some name"}
    @update_attrs %{misc: %{}, name: "some updated name"}
    @invalid_attrs %{misc: nil, name: nil}

    def tracker_fixture(attrs \\ %{}) do
      {:ok, tracker} =
        attrs
        |> Enum.into(@valid_attrs)
        |> SMS.create_tracker()

      tracker
    end

    test "list_trackers/0 returns all trackers" do
      tracker = tracker_fixture()
      assert SMS.list_trackers() == [tracker]
    end

    test "get_tracker!/1 returns the tracker with given id" do
      tracker = tracker_fixture()
      assert SMS.get_tracker!(tracker.id) == tracker
    end

    test "create_tracker/1 with valid data creates a tracker" do
      assert {:ok, %Tracker{} = tracker} = SMS.create_tracker(@valid_attrs)
      assert tracker.misc == %{}
      assert tracker.name == "some name"
    end

    test "create_tracker/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = SMS.create_tracker(@invalid_attrs)
    end

    test "update_tracker/2 with valid data updates the tracker" do
      tracker = tracker_fixture()
      assert {:ok, tracker} = SMS.update_tracker(tracker, @update_attrs)
      assert %Tracker{} = tracker
      assert tracker.misc == %{}
      assert tracker.name == "some updated name"
    end

    test "update_tracker/2 with invalid data returns error changeset" do
      tracker = tracker_fixture()
      assert {:error, %Ecto.Changeset{}} = SMS.update_tracker(tracker, @invalid_attrs)
      assert tracker == SMS.get_tracker!(tracker.id)
    end

    test "delete_tracker/1 deletes the tracker" do
      tracker = tracker_fixture()
      assert {:ok, %Tracker{}} = SMS.delete_tracker(tracker)
      assert_raise Ecto.NoResultsError, fn -> SMS.get_tracker!(tracker.id) end
    end

    test "change_tracker/1 returns a tracker changeset" do
      tracker = tracker_fixture()
      assert %Ecto.Changeset{} = SMS.change_tracker(tracker)
    end
  end

  describe "data" do
    alias Sleepweb.SMS.Data

    @valid_attrs %{angular_change: 120.5, bpm: 120.5, event: "some event", ibi: 120.5, lin_acc: 120.5, micro_vol: 120.5, misc: "some misc", sensor: "some sensor", time: "2010-04-17 14:00:00.000000Z"}
    @update_attrs %{angular_change: 456.7, bpm: 456.7, event: "some updated event", ibi: 456.7, lin_acc: 456.7, micro_vol: 456.7, misc: "some updated misc", sensor: "some updated sensor", time: "2011-05-18 15:01:01.000000Z"}
    @invalid_attrs %{angular_change: nil, bpm: nil, event: nil, ibi: nil, lin_acc: nil, micro_vol: nil, misc: nil, sensor: nil, time: nil}

    def data_fixture(attrs \\ %{}) do
      {:ok, data} =
        attrs
        |> Enum.into(@valid_attrs)
        |> SMS.create_data()

      data
    end

    test "list_data/0 returns all data" do
      data = data_fixture()
      assert SMS.list_data() == [data]
    end

    test "get_data!/1 returns the data with given id" do
      data = data_fixture()
      assert SMS.get_data!(data.id) == data
    end

    test "create_data/1 with valid data creates a data" do
      assert {:ok, %Data{} = data} = SMS.create_data(@valid_attrs)
      assert data.angular_change == 120.5
      assert data.bpm == 120.5
      assert data.event == "some event"
      assert data.ibi == 120.5
      assert data.lin_acc == 120.5
      assert data.micro_vol == 120.5
      assert data.misc == "some misc"
      assert data.sensor == "some sensor"
      assert data.time == DateTime.from_naive!(~N[2010-04-17 14:00:00.000000Z], "Etc/UTC")
    end

    test "create_data/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = SMS.create_data(@invalid_attrs)
    end

    test "update_data/2 with valid data updates the data" do
      data = data_fixture()
      assert {:ok, data} = SMS.update_data(data, @update_attrs)
      assert %Data{} = data
      assert data.angular_change == 456.7
      assert data.bpm == 456.7
      assert data.event == "some updated event"
      assert data.ibi == 456.7
      assert data.lin_acc == 456.7
      assert data.micro_vol == 456.7
      assert data.misc == "some updated misc"
      assert data.sensor == "some updated sensor"
      assert data.time == DateTime.from_naive!(~N[2011-05-18 15:01:01.000000Z], "Etc/UTC")
    end

    test "update_data/2 with invalid data returns error changeset" do
      data = data_fixture()
      assert {:error, %Ecto.Changeset{}} = SMS.update_data(data, @invalid_attrs)
      assert data == SMS.get_data!(data.id)
    end

    test "delete_data/1 deletes the data" do
      data = data_fixture()
      assert {:ok, %Data{}} = SMS.delete_data(data)
      assert_raise Ecto.NoResultsError, fn -> SMS.get_data!(data.id) end
    end

    test "change_data/1 returns a data changeset" do
      data = data_fixture()
      assert %Ecto.Changeset{} = SMS.change_data(data)
    end
  end
end
