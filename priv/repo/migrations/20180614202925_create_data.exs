defmodule Sleepweb.Repo.Migrations.CreateData do
  use Ecto.Migration

  def change do
    create table(:data) do
      add :sensor, :string
      add :time, :utc_datetime
      add :bpm, :float
      add :ibi, :float
      add :lin_acc, :float
      add :micro_vol, :float
      add :angular_change, :float
      add :event, :string
      add :misc, :string

      timestamps()
    end

  end
end
